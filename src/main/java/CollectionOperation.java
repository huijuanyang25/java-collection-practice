import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CollectionOperation {
    
    public static List<Integer> getListByInterval(int left, int right) {
        // Need to be implemented
        List<Integer> getList;
        if (left < right) {
            getList = Stream.iterate(left, integer -> integer + 1)
                    .limit(right - left + 1)
                    .collect(Collectors.toList());
        } else {
            getList = Stream.iterate(left, integer -> integer - 1)
                    .limit(left - right + 1)
                    .collect(Collectors.toList());
        }
        return getList;
    }
    
    
    public static List<Integer> removeLastElement(List<Integer> list) {
        // Need to be implemented
        return list.stream().limit(list.size() - 1).collect(Collectors.toList());
    }
    
    public static List<Integer> sortDesc(List<Integer> list) {
        // Need to be implemented
        return list.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
    }
    
    
    public static List<Integer> reverseList(List<Integer> list) {
        // Need to be implemented
        Collections.reverse(list);
        return list;
    }
    
    
    public static List<Integer> concat(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
        return Stream.concat(list1.stream(), list2.stream()).collect(Collectors.toList());
    }
    
    public static List<Integer> union(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
        return Stream.concat(list1.stream(), list2.stream())
                .sorted()
                .distinct()
                .collect(Collectors.toList());
    }
    
    public static boolean isAllElementsEqual(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
        List<Integer> sortedList1 = list1.stream().sorted().collect(Collectors.toList());
        List<Integer> sortedList2 = list2.stream().sorted().collect(Collectors.toList());
        return sortedList1.equals(sortedList2);
    }
}
