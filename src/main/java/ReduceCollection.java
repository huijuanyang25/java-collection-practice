import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

public class ReduceCollection {
    public static int getMax(List<Integer> list) {
        // Need to be implemented
        int maxNumber = list.stream().reduce(0, Integer::max);
        return maxNumber;
    }
    
    public static double getAverage(List<Integer> list) {
        // Need to be implemented
        int sum = list.stream().reduce(0, Integer::sum);
        double average = sum / list.size();
        return average;
    }
    
    public static int getSum(List<Integer> list) {
        // Need to be implemented
        int sum = list.stream().reduce(0, Integer::sum);
        return sum;
    }
    
    public static double getMedian(List<Integer> list) {
        // Need to be implemented
        double median;
        if (list.size() % 2 == 0) {
            median = (list.get(list.size() / 2 - 1) + list.get(list.size() / 2)) / 2;
        } else {
            median = list.get((list.size() - 1) / 2);
        }
        return median;
    }
    
    public static int getFirstEven(List<Integer> list) {
        // Need to be implemented
        return list.stream().filter(element -> element % 2 == 0).sorted().collect(Collectors.toList()).get(0);
    }
}
